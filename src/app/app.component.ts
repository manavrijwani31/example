import { Component} from '@angular/core';
import { PostService } from './services/post.service';
import { CountryService } from './services/country.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = '2.Services';

  // posts:any;  //making a variable named post and its type is any

  // constructor(private post:PostService) {} //post is a parameter of PostService type

  // ngOnInit(){
  //   this.post.getPosts().subscribe((response: any) => {
  //     this.posts = response;
  //     console.log(this.posts);
  //   })
  // }

  countries: any[] = [];

  constructor(private countryService: CountryService) { }

  ngOnInit() {
    this.countryService.getCountries().subscribe((data: any[]) => {
      this.countries = data;
      console.log(this.countries);
    },
    (error: any) => {
      console.error('An error occurred:', error);
    }
    );
  }


}
